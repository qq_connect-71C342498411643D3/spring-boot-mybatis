package com.xyxs.demo;

/**
 * @Description:
 * @Author: zhaichen
 * @Date: 2018-08-08 13:47
 * @Version: 1.0
 * @Created in idea by autoCode
 */
public class Test {

    public static void main(String[] args) {
        int[] a = {2, 5, 6, 7, 9};

        int[] b = {2, 3, 4, 7};

        int[] c = new int[9];

        int i = 0;

        int j = 0;

        int k = 0;

        int leng1 = 5;

        int leng2 = 4;

        while (i < leng1 && j < leng2){
            if(a[i] <= b[j]){
                a[i++] = c[k++];
            }else{
                b[j++] = c[k++];
            }
        }

        if(i > leng1){
            while(j < leng2){
                b[j++] = c[k++];
            }
            System.out.println("j =" + j);
            System.out.println("b[j] = "+ b[j++]);
        }

        if(j > leng2){
            while (i < leng1){
                a[i++] = c[k++];
            }
            System.out.println("i =" + i);
            System.out.println("a[i] = " + a[i++]);
        }

    }

}
