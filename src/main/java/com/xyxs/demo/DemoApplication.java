package com.xyxs.demo;

import com.xyxs.demo.config.Config;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
@RestController
@SpringBootApplication
@MapperScan(basePackages = {"com.xyxs.demo.dao"})
@ComponentScan(basePackages = "com.xyxs")
public class DemoApplication {

	@RequestMapping("/")
	public String home(){
		Config config = new Config();
		System.out.println(config.getName());
		List<String> stringList = config.getServers();
		for (String string: stringList) {
			System.out.println(string);
		}
		return "hello world";
	}

	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}
