package com.xyxs.demo.service;

import com.xyxs.demo.dto.request.SysUserAddRequestDto;
import com.xyxs.demo.dto.response.SysUserEditResponseDto;
import com.xyxs.demo.dto.response.SysUserResponseDto;
import com.xyxs.demo.dto.response.SysUserResponseListDto;
import com.xyxs.demo.model.SysUser;

/**
 * @Description:
 * @Author: zhaichen
 * @Date: 2017-12-12 16:38
 * @Version: 1.0
 * @Created in idea by autoCode
 */
public interface SysUserService {
    /**
     * 根据id获取用户
     * @param id
     * @return
     */
    SysUserResponseDto getById(Long id);

    /**
     * 根据id获取需修改的用户
     * @param id
     * @return
     */
    SysUserEditResponseDto getEditById(Long id);

    /**
     * 添加用户
     * @param sysUserAddRequestDto
     * @return
     */
    SysUserResponseDto sysUserAdd(SysUserAddRequestDto sysUserAddRequestDto);

    /**
     * 获取所有用户信息
     * @return
     */
    SysUserResponseListDto getSysUserAll();

    String getString();

    /**
     * 根据用户名获取用户信息
     * @param userName
     * @return
     */
    SysUser getByUserName(String userName);

    /**
     * 保存用户信息
     * @param sysUser
     * @return
     */
    Integer save(SysUser sysUser);

}
