package com.xyxs.demo.service.impl;

import com.xyxs.demo.common.util.DozerUtil;
import com.xyxs.demo.dao.SysUserMapper;
import com.xyxs.demo.dto.request.SysUserAddRequestDto;
import com.xyxs.demo.dto.response.SysUserResponseListDto;
import com.xyxs.demo.model.SysUser;
import com.xyxs.demo.service.SysUserService;
import com.xyxs.demo.dto.response.SysUserEditResponseDto;
import com.xyxs.demo.dto.response.SysUserResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description:
 * @Author: zhaichen
 * @Date: 2017-12-12 16:38
 * @Version: 1.0
 * @Created in idea by autoCode
 */
@Service
public class SysUserServiceImpl implements SysUserService {

    @Autowired
    private SysUserMapper sysUserMapper;

    @Override
    public SysUserResponseDto getById(Long id) {
        SysUser sysUser = sysUserMapper.selectByPrimaryKey(id);
        if(sysUser!=null){
            SysUserResponseDto sysUserResponseDto = DozerUtil.transfer(sysUser, SysUserResponseDto.class);
//            Gender gender = Gender.values()[sysUser.getSex()];
//            sysUserResponseDto.setSex(gender.getValue());
//            Enable enable = Enable.codeOf(sysUser.getEnable());
//            sysUserResponseDto.setEnable(enable.getValue());

            return sysUserResponseDto;
        }
        return null;
    }

    @Override
    public SysUserEditResponseDto getEditById(Long id) {
        SysUser sysUser = sysUserMapper.selectByPrimaryKey(id);
        if(sysUser!=null) {
            SysUserEditResponseDto sysUserEditResponseDto = DozerUtil.transfer(sysUser, SysUserEditResponseDto.class);
            return sysUserEditResponseDto;
        }
        return null;
    }

    @Override
    public SysUserResponseDto sysUserAdd(SysUserAddRequestDto sysUserAddRequestDto) {
        if(sysUserAddRequestDto != null){
            SysUser sysUser = DozerUtil.transfer(sysUserAddRequestDto, SysUser.class);
            System.out.println(sysUser);

            SysUserResponseDto sysUserResponseDto = DozerUtil.transfer(sysUser, SysUserResponseDto.class);
//            Gender gender = Gender.values()[sysUser.getSex()];
//            sysUserResponseDto.setSex(gender.getValue());
//            Enable enable = Enable.codeOf(sysUser.getEnable());
//            sysUserResponseDto.setEnable(enable.getValue());
            return sysUserResponseDto;
        }
        return null;
    }

    @Override
    public SysUserResponseListDto getSysUserAll() {
//        List<SysUser> sysUserList = sysUserMapper.selectAll();
        SysUserResponseListDto sysUserResponseListDto = new SysUserResponseListDto();

        SysUser sysUser = new SysUser();
        sysUser.setUsername("李四");
        sysUser.setPassword("123456");
        List<SysUser> sysUserList = new ArrayList<>();
        sysUserList.add(sysUser);

        sysUserResponseListDto.setSysUserResponseDtoList(DozerUtil.transfer(sysUserList, List.class));
        return sysUserResponseListDto;
    }

    @Override
    public String getString() {
        return "testString";
    }

    @Override
    public SysUser getByUserName(String userName) {
        SysUser sysUser = sysUserMapper.selectByUsername(userName);
        return sysUser;
    }

    @Override
    public Integer save(SysUser sysUser) {
        int i = sysUserMapper.insert(sysUser);
        return i;
    }
}
