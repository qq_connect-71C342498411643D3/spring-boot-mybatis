package com.xyxs.demo.service.impl;

import org.springframework.security.core.GrantedAuthority;

/**
 * @Description:
 * @Author: zhaichen
 * @Date: 2018-09-14 11:08
 * @Version: 1.0
 * @Created in idea by autoCode
 */
public class GrantedAuthorityImpl implements GrantedAuthority {

    private String authority;

    public GrantedAuthorityImpl(String authority) {
        this.authority = authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    @Override
    public String getAuthority() {
        return this.authority;
    }

}
