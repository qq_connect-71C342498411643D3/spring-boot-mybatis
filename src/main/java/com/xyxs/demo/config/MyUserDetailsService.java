package com.xyxs.demo.config;

import com.xyxs.demo.dao.SysRoleMapper;
import com.xyxs.demo.dao.SysUserMapper;
import com.xyxs.demo.model.SysRole;
import com.xyxs.demo.model.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description:
 * @Author: zhaichen
 * @Date: 2018-07-11 16:36
 * @Version: 1.0
 * @Created in idea by autoCode
 */
@Service
public class MyUserDetailsService implements UserDetailsService {

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private SysRoleMapper sysRoleMapper;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        System.out.println("s:"+s);
        SysUser user = sysUserMapper.selectByUsername(s);

        if(user == null) {
            throw new UsernameNotFoundException("用户名不存在");
        }

        List<SimpleGrantedAuthority> authorities = new ArrayList<>();

        List<SysRole> sysRoles = sysRoleMapper.selectByUserId(user.getId());
        for (SysRole sysRole: sysRoles) {
            authorities.add(new SimpleGrantedAuthority(sysRole.getName()));
        }

        System.out.println("username:"+user.getUsername()+"; password: "+user.getPassword());
        return new User(user.getUsername(), user.getPassword(), authorities);
    }
}
