package com.xyxs.demo.config;

import com.xyxs.demo.handler.CustomAccessDeniedHandler;
import com.xyxs.demo.handler.CustomAuthenticationSuccessHandler;
import com.xyxs.demo.handler.CustomLogoutSuccessHandler;
import com.xyxs.demo.service.impl.CustomAuthenticationProvider;
import com.xyxs.demo.web.filter.JWTAuthenticationFilter;
import com.xyxs.demo.web.filter.JWTLoginFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.LogoutConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @Description:
 * @Author: zhaichen
 * @Date: 2018-07-11 15:51
 * @Version: 1.0
 * @Created in idea by autoCode
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    /**
     * 需要放行的URL
     */
    private static final String[] AUTH_WHITELIST = {
            // -- register url
            "/users/signup",
            // -- swagger ui
            "/v2/api-docs",
            "/swagger-resources",
            "/swagger-resources/**",
            "/configuration/ui",
            "/configuration/security",
            "/swagger-ui.html",
            "/webjars/**",
            // other public endpoints of your API may be appended to this array
            "/test/login",
            "/websocket/**",
            "/users/signUp"
    };

//    @Bean
//    UserDetailsService myUserDetailsService() {
//        return new MyUserDetailsService();
//    }

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private CustomAccessDeniedHandler customAccessDeniedHandler;

    @Autowired
    private CustomLogoutSuccessHandler customLogoutSuccessHandler;

    @Autowired
    private CustomAuthenticationSuccessHandler customAuthenticationSuccessHandler;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        LogoutConfigurer<HttpSecurity> httpSecurityLogoutConfigurer = http.cors().and().csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .authorizeRequests()
                .antMatchers(AUTH_WHITELIST).permitAll()
                .anyRequest().authenticated()   // 所有请求需要身份认证
                .and()
                .exceptionHandling().accessDeniedHandler(customAccessDeniedHandler) // 自定义访问失败处理器
                .and()
                .addFilter(new JWTLoginFilter(authenticationManager()))
                .addFilter(new JWTAuthenticationFilter(authenticationManager()))
                .logout() // 默认注销行为为logout，可以通过下面的方式来修改
                .logoutUrl("/logout")
                .logoutSuccessUrl("/login")// 设置注销成功后跳转页面，默认是跳转到登录页面;
                .logoutSuccessHandler(customLogoutSuccessHandler)
                .permitAll();

//                .formLogin().loginPage("/test/login")
//                //设置默认登录成功跳转界面
//                .defaultSuccessUrl("/test/index").failureUrl("/test/login?error").permitAll()
//                .and()
//                .addFilter(new JWTLoginFilter(authenticationManager()))
//                .addFilter(new JWTAuthenticationFilter(authenticationManager()))
//                .logout()
//                .logoutUrl("/logout")
//                .logoutSuccessUrl("/login")
//                .logoutSuccessHandler(customLogoutSuccessHandler)
//                .permitAll();

//        http.authorizeRequests()
//                .anyRequest().authenticated()
//                .and().formLogin().loginPage("/test/login")
//                //设置默认登录成功跳转界面
//                .defaultSuccessUrl("/test/index").failureUrl("/test/login?error").permitAll()
//                .and()
//                .logout()
//                .permitAll();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/js/**", "/css/**", "/images/**", "/**/favicon.ico", "/webjars/**", "/vendor/**"); //静态资源地址
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        /*auth
                .inMemoryAuthentication()
                .withUser("userName").password("passWord").roles("USER", "USER2");*/  //配置的用户信息地址
        /*auth.userDetailsService(myUserDetailsService()).passwordEncoder(new BCryptPasswordEncoder());*/
        auth.authenticationProvider(new CustomAuthenticationProvider(userDetailsService, bCryptPasswordEncoder));

    }

}
