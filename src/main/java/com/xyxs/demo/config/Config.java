package com.xyxs.demo.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description:
 * @Author: zhaichen
 * @Date: 2017-12-12 14:57
 * @Version: 1.0
 * @Created in idea by autoCode
 */
@ConfigurationProperties(prefix = "my")
public class Config {
    private String name;
    private Integer port;
    private List<String> servers = new ArrayList();


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public List<String> getServers() {
        return servers;
    }

    public void setServers(List<String> servers) {
        this.servers = servers;
    }

}
