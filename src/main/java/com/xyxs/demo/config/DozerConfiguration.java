package com.xyxs.demo.config;

import org.dozer.DozerBeanMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.List;

/**
 * @Description:
 * @Author: zhaichen
 * @Date: 2017-12-14 10:32
 * @Version: 1.0
 * @Created in idea by autoCode
 */
@Configuration
public class DozerConfiguration {

    @Bean(name = "org.dozer.Mapper")
    public static DozerBeanMapper dozerBean() {
        List<String> mappingFiles = Arrays.asList(
          "dozer-bean-mappings.xml"
        );

        DozerBeanMapper dozerBeanMapper = new DozerBeanMapper();
        dozerBeanMapper.setMappingFiles(mappingFiles);
        return dozerBeanMapper;
    }
}
