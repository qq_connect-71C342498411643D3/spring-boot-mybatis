package com.xyxs.demo.exception;

/**
 * @Description:
 * @Author: zhaichen
 * @Date: 2018-09-14 11:04
 * @Version: 1.0
 * @Created in idea by autoCode
 */
public class UsernameIsExitedException extends BaseException {

    public UsernameIsExitedException(String msg) {
        super(msg);
    }

    public UsernameIsExitedException(String msg, Throwable t) {
        super(msg, t);
    }
}
