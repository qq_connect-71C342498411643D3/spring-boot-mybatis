package com.xyxs.demo.exception;

/**
 * @Description:
 * @Author: zhaichen
 * @Date: 2018-09-14 11:03
 * @Version: 1.0
 * @Created in idea by autoCode
 */
public class TokenException extends BaseException {

    private static final long serialVersionUID = 1L;

    public TokenException(String message) {
        super(message);
    }

}
