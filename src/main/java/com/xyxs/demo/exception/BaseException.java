package com.xyxs.demo.exception;

/**
 * @Description:
 * @Author: zhaichen
 * @Date: 2018-09-14 11:01
 * @Version: 1.0
 * @Created in idea by autoCode
 */
public class BaseException extends RuntimeException {

    public BaseException(String message) {
        super(message);
    }

    public BaseException(String message, Throwable cause) {
        super(message, cause);
    }

}
