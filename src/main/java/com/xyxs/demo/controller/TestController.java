package com.xyxs.demo.controller;

import com.alibaba.fastjson.JSONObject;
import com.xyxs.demo.WebSocket.WebSocketServer;
import com.xyxs.demo.common.CommonResult;
import com.xyxs.demo.common.ThymeleafTest;
import com.xyxs.demo.common.util.DozerUtil;
import com.xyxs.demo.dto.response.SysUserResponseDto;
import com.xyxs.demo.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @Description:
 * @Author: zhaichen
 * @Date: 2018-07-11 10:54
 * @Version: 1.0
 * @Created in idea by autoCode
 */
@Controller
@RequestMapping("test")
public class TestController {

    @Autowired
    private SysUserService sysUserService;

    @RequestMapping(value = "test")
    public String test(Model model){
        SysUserResponseDto sysUserResponseDto = sysUserService.getById(new Long("1"));
        sysUserResponseDto.setName("张三");
        sysUserResponseDto.setPhone("13112121212");
        model.addAttribute("sysUserResponseDto", sysUserResponseDto);
        model.addAttribute("DozerUtil", new DozerUtil());
        Map map = new HashMap();
        map.put("sysUserResponseDto", sysUserResponseDto);
        map.put("array", new String[]{"土豆", "番茄", "白菜", "芹菜"});
        map.put("DozerUtil", new DozerUtil());
//        ThymeleafTest.thyInexToStatic1();

        try {
            WebSocketServer.sendInfo("测试发送通知");
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "example";
    }

    @RequestMapping(value = "index")
    public String index(Model model){
        SysUserResponseDto sysUserResponseDto = sysUserService.getById(new Long("1"));
        model.addAttribute("sysUserResponseDto", sysUserResponseDto);
        return "index";
    }

//    @RequestMapping(value = "login")
//    @ResponseBody
//    public CommonResult login(String userName, String passWord){
//        JSONObject jsonObject = new JSONObject();
//        jsonObject.put("userName", userName);
//        jsonObject.put("passWord", passWord);
//        return CommonResult.success("登录成功", jsonObject);
//    }

    @RequestMapping(value = "login")
    public String login(String userName, String passWord){
        return "login";
    }

}
