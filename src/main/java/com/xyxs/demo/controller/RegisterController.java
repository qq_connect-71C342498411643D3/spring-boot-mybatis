package com.xyxs.demo.controller;

import com.xyxs.demo.common.CommonResult;
import com.xyxs.demo.model.SysUser;
import com.xyxs.demo.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description:
 * @Author: zhaichen
 * @Date: 2018-09-14 15:02
 * @Version: 1.0
 * @Created in idea by autoCode
 */
@RestController
@RequestMapping("users")
public class RegisterController extends BaseController {

    @Autowired
    private SysUserService sysUserService;

    @RequestMapping("/signUp")
    public CommonResult signUp(@RequestBody SysUser sysUser) {

        SysUser bizSysUser = sysUserService.getByUserName(sysUser.getUsername());

        if(null != bizSysUser) {
//            throw new UsernameIsExitedException("用户已经存在");
            return CommonResult.failed("用户已经存在", null);
        }

        sysUser.setPassword(bCryptPasswordEncoder.encode(sysUser.getPassword()));

        int i = sysUserService.save(sysUser);

        if(0 == i) {
            return CommonResult.failed("保存用户失败，请联系管理员", null);
        }
        return CommonResult.success("保存成功", sysUser);
    }

}
