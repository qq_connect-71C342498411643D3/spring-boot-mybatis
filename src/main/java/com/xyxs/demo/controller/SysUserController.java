package com.xyxs.demo.controller;

import com.xyxs.demo.common.CommonResult;
import com.xyxs.demo.dto.request.SysUserAddRequestDto;
import com.xyxs.demo.dto.response.SysUserResponseListDto;
import com.xyxs.demo.service.SysUserService;
import com.xyxs.demo.dto.response.SysUserEditResponseDto;
import com.xyxs.demo.dto.response.SysUserResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description:
 * @Author: zhaichen
 * @Date: 2017-12-12 16:47
 * @Version: 1.0
 * @Created in idea by autoCode
 */
@RestController
@RequestMapping(value = "sysUser")
public class SysUserController {
    @Autowired
    private SysUserService sysUserService;

    @RequestMapping(value = "/getById/{id}")
    public CommonResult getById(@PathVariable Long id){
        List<SysUserResponseDto> sysUserList = new ArrayList();
        sysUserList.add(sysUserService.getById(id));
        return CommonResult.success("获取数据成功", sysUserList);
    }

    @RequestMapping(value = "/getSysUserEditById1/{id}")
    public CommonResult getSysUserEditById1(@PathVariable Long id){
        List<SysUserEditResponseDto> sysUserEditVoDtoList = new ArrayList();
        sysUserEditVoDtoList.add(sysUserService.getEditById(id));
        return CommonResult.success("获取需编辑的数据成功", sysUserEditVoDtoList);
    }

    @RequestMapping(value = "/sysUserAdd")
    public CommonResult sysUserAdd(SysUserAddRequestDto sysUserAddRequestDto){
        SysUserResponseDto sysUserResponseDto = sysUserService.sysUserAdd(sysUserAddRequestDto);
        return CommonResult.success("保存成功", sysUserResponseDto);
    }

    @RequestMapping(value = "/getSysUserAll")
    public CommonResult getSysUserAll(){
        SysUserResponseListDto sysUserResponseListDto = sysUserService.getSysUserAll();
        return CommonResult.success("获取成功", sysUserResponseListDto);
    }

}
