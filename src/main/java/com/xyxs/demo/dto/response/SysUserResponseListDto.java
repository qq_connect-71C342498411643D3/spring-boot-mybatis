package com.xyxs.demo.dto.response;

import java.util.ArrayList;
import java.util.List;

public class SysUserResponseListDto {

    public List<SysUserResponseDto> sysUserResponseDtoList = new ArrayList();

    public List<SysUserResponseDto> getSysUserResponseDtoList() {
        return sysUserResponseDtoList;
    }

    public void setSysUserResponseDtoList(List<SysUserResponseDto> sysUserResponseDtoList) {
        this.sysUserResponseDtoList = sysUserResponseDtoList;
    }
}