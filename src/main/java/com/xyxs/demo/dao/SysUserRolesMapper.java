package com.xyxs.demo.dao;

import com.xyxs.demo.model.SysUserRoles;

public interface SysUserRolesMapper {
    int insert(SysUserRoles record);

    int insertSelective(SysUserRoles record);
}