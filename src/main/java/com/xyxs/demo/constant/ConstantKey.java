package com.xyxs.demo.constant;

/**
 * @Description:
 * @Author: zhaichen
 * @Date: 2018-09-14 11:06
 * @Version: 1.0
 * @Created in idea by autoCode
 */
public class ConstantKey {

    /**
     * 签名key
     */
    public static final String SIGNING_KEY = "spring-security-@Jwt!&Secret^#";

}
