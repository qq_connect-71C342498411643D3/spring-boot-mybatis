package com.xyxs.demo.common.util;

import com.xyxs.demo.config.DozerConfiguration;
import org.dozer.Mapper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @Description:
 * @Author: zhaichen
 * @Date: 2017-12-13 16:48
 * @Version: 1.0
 * @Created in idea by autoCode
 */
public class DozerUtil {
    private static final Mapper mapper = DozerConfiguration.dozerBean();

    public static <T> T transfer(Object source, Class<T> destination) {
        return mapper.map(source, destination);
    }

    public static <T> List<T> transfer(Collection<Object> collections, Class<T> destination) {
        List<T> list = new ArrayList<T>();
        for (Object source : collections) {
            list.add(mapper.map(source, destination));
        }
        return list;
    }

    public static void transfer(Object source, Object destination) {
        mapper.map(source, destination);
    }

    public static String test(){
        return "thymeleaf调用java方法成功";
    }

}
