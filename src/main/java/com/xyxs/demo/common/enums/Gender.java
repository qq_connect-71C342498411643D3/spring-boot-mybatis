package com.xyxs.demo.common.enums;

/**
 * @Description:
 * @Author: zhaichen
 * @Date: 2017-12-13 16:57
 * @Version: 1.0
 * @Created in idea by autoCode
 */
public enum Gender {
    Boy(1, "男"),

    Girl(0, "女"),

    Unknow(2, "未知");

    Integer code;
    String value;

    Gender(Integer code, String value) {
        this.code = code;
        this.value = value;
    }

    public Integer getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

}
