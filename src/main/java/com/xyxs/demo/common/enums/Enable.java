package com.xyxs.demo.common.enums;

/**
 * @Description:
 * @Author: zhaichen
 * @Date: 2017-12-14 9:05
 * @Version: 1.0
 * @Created in idea by autoCode
 */
public enum Enable {
    ENABLE(1, "正常"),

    DISENABLE(0, "禁用"),

    DELETE(-1, "已删除");

    Integer code;
    String value;

    Enable(Integer code, String value) {
        this.code = code;
        this.value = value;
    }

    public static Enable codeOf(int code) {
        switch (code){
            case 1:
                return Enable.ENABLE;
            case 2:
                return Enable.DISENABLE;
            case -1:
                return Enable.DELETE;
            default:
                return null;
        }
    }

    public Integer getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }
}
