package com.xyxs.demo.common;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

/**
 * @Description:
 * @Author: zhaichen
 * @Date: 2018-07-12 15:19
 * @Version: 1.0
 * @Created in idea by autoCode
 */
public class ThymeleafTest {

    public static void thyIndexToStatic() {
        try {
            HttpClient client = new HttpClient();
            GetMethod getMethod = new GetMethod("http://127.0.0.1:8080/test/test");//首页访问路径
            String path=ThymeleafTest.class.getClassLoader().getResource("").getPath();
            String path2 = path.replace("/target/classes/", "/src/main/resources/templates/");
            client.executeMethod(getMethod);
            File file = new File(path2+"test1.html");//存到/src/main/resources/templates 下
            FileWriter writer = new FileWriter(file);
            writer.write(getMethod.getResponseBodyAsString());
            writer.flush();
            System.err.println("首页静态化已完成...");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void thyInexToStatic1(Map map) {
        ClassLoaderTemplateResolver resolver = new ClassLoaderTemplateResolver();
        resolver.setPrefix("templates/"); //模板所在目录，相对于当前classloader的classpath。
        resolver.setSuffix(".html"); //模板文件后缀
        TemplateEngine templateEngine = new TemplateEngine();
        templateEngine.setTemplateResolver(resolver);

        Context context = new Context();
//        context.setVariable("name", "蔬菜列表");
//        context.setVariable("array", new String[]{"土豆", "番茄", "白菜", "芹菜"});
        context.setVariables(map);

        String path=ThymeleafTest.class.getClassLoader().getResource("").getPath();
        String path2 = path.replace("/target/classes/", "/src/main/resources/templates/");

        //渲染模板
        FileWriter write = null;
        try {
            write = new FileWriter(path2 + "result.html");
        } catch (IOException e) {
            e.printStackTrace();
        }
        templateEngine.process("example", context, write);

    }

    public static void main(String[] args) {
        thyIndexToStatic();
    }

}
