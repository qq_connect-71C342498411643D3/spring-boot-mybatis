package com.xyxs.demo.common;

import java.io.Serializable;

/**
 * @Description:
 * @Author: zhaichen
 * @Date: 2017-12-13 14:11
 * @Version: 1.0
 * @Created in idea by autoCode
 */
public class CommonResult implements Serializable {
    // 相应业务状态
    private Integer status;

    // 相应消息
    private String message;

    // 相应数据
    private Object data;

    public static CommonResult success(String message, Object data){
        return new CommonResult(1, message, data);
    }

    public static CommonResult failed(String message, Object data){
        return new CommonResult(0, message, data);
    }

    public CommonResult() {
    }

    public CommonResult(Integer status, String message, Object data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
